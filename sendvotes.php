<?php
  function CallAPI($url, $data = false)
  {
      $curl = curl_init();
  
      curl_setopt($curl, CURLOPT_POST, 1);
  
      if ($data)
          curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
  
      /*
      // Optional Authentication:
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($curl, CURLOPT_USERPWD, "username:password");
      */
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      
      $result = curl_exec($curl);
      $status = curl_getinfo($curl,  CURLINFO_HTTP_CODE);
      
      curl_close($curl);
      
      $ret = array($result,$status);
      
      return $ret;
  } 

  $data = array();
  $data['used'] = "yes";
  $data['email'] = $_POST['email'];

  $reply = CallAPI("http://95.110.196.250/weyspace/json.php",$data);
  
  $credits_obj = json_decode($reply[0],true);

  $credits = $credits_obj[0]["Credits"];
  
  $bid = intval($_POST['vote_credits']) + intval($_POST['with_credits']); 
  
  if($bid <= intval($credits))
  {
    $data = array();
    $data['credits'] = $credits;
    $data['bid'] = (string)($bid);
    $data['email'] = $_POST['email'];
    $data['dec'] = "yes";
    
    $reply = CallAPI("http://95.110.196.250/weyspace/credits_used.php",$data);
    
    $data = array();
    $data['nome'] = $_POST['name'];
    $data['email'] = $_POST['email'];
    $data['udid'] = $_POST['udid'];
    $data['pic'] = $_POST['picture'];
    $data['credits'] = $_POST['vote_credits'];
    
    $reply = CallAPI("http://95.110.196.250/weyspace/put_a_vote.php",$data);
    
    if(strcmp($_POST['with_credits'],"")!=0 && strcmp($_POST['with_credits'],"0")!=0){
    $data = array();
    $data['udid'] = $_POST['udid'];
    $data['email'] = $_POST['email'];
    $data['credits'] = $_POST['with_credits'];
    
    $reply = CallAPI("http://95.110.196.250/weyspace/go_with.php",$data);

    }
    
    echo "OK";
  }
  else
  {
    echo "NOK";
  }
?>