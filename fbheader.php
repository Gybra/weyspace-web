<div id="fb-root"></div>
<script type="text/javascript">
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '<?php echo AppInfo::appID(); ?>', // App ID
      channelUrl : '//<?php echo $_SERVER["HTTP_HOST"]; ?>/channel.html', // Channel File
      status     : true, // check login status
      cookie     : true, // enable cookies to allow the server to access the session
      xfbml      : true // parse XFBML
    });

    // Listen to the auth.login which will be called when the user logs in
    // using the Login button
    FB.Event.subscribe('auth.login', function(response) {
      // We want to reload the page now so PHP can read the cookie that the
      // Javascript SDK sat. But we don't want to use
      // window.location.reload() because if this is in a canvas there was a
      // post made to this page and a reload will trigger a message to the
      // user asking if they want to send data again.
      window.location = window.location;
    });

    FB.Canvas.setAutoGrow();
  };

  // Load the SDK Asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/all.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
</script>

<?
//echo $fix_script;
/*
 if (isset($_GET['setdefaultcookie'])) {
        // top level page, set default cookie then redirect back to canvas page
        setcookie ('default',"1",0,"/");
        $url = substr($_SERVER['REQUEST_URI'],strrpos($_SERVER['REQUEST_URI'],"/")+1);
        $url = str_replace("setdefaultcookie","defaultcookieset",$url);
        $url = $facebookapp->getCanvasUrl($url);
        echo "<html>\n<body>\n<script>\ntop.location.href='".$url."';\n</script></body></html>";
        exit();
    } else if ((!isset($_COOKIE['default'])) && (!isset($_GET['defaultcookieset']))) {
        // no default cookie, so we need to redirect to top level and set
        $url = $_SERVER['REQUEST_URI'];
        if (strpos($url,"?") === false) $url .= "?";
        else $url .= "&";
        $url .= "setdefaultcookie=1";
        echo "<html>\n<body>\n<script>\ntop.location.href='".$url."';\n</script></body></html>";
        exit();
    }*/
    
    
 ?>
 
<?php if(count($_COOKIE) == 0) { ?>
<a id="safari_cookie_link" href="safari_cookie_fix.php" target="_blank" style="display:none;">Click here to load content</a>
<script type="text/javascript">
	$(function(){
		$.('safari_cookie_link').click();
	});
</script>
<?php } ?>