<?php
  require_once('phpheader.php'); 

  $data = array();
  $data['name'] = $_SESSION['regdata']['first_name'];
  $data['surname'] = $_SESSION['regdata']['last_name'];
  $data['nazione'] = $_SESSION['regdata']['country'];
  $data['birth'] = $_SESSION['regdata']['birthdayJS'];
  $data['gender'] = $_SESSION['regdata']['gender'];
  $data['email'] = $_SESSION['regdata']['email'];
  $data['password'] = $_SESSION['regdata']['password'];
  
  $reply = CallAPI("http://95.110.196.250/weyspace/put_a_user.php",$data);
  
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>
      registration
    </title>
    <script src="jquery-1.9.1.min.js" type="text/javascript">
    </script>
  </head>
  <body>
    <?php require_once('fbheader.php'); ?>
    <script type="text/javascript">
        $(document).ready(function ()
          {
            window.location.replace('login.php');
          });
    </script>
  </body>
</html>