<?php
session_start();
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');


    // START SAFARI SESSION FIX
    
 
    
    // END SAFARI SESSION FIX
    
//echo " ".session_id()." ";
require_once('AppInfo.php');

function idx(array $array, $key, $default = null) {
  return array_key_exists($key, $array) ? $array[$key] : $default;
}

function he($str) {
  return htmlentities($str, ENT_COMPAT, "UTF-8");
}

function html_escape_tags($str)
{
  $ret=str_replace(array("<",">"),array("&lt;","&gt;"),$str);
  return $ret;
}

function html_escape_quotes($str,$both)
{
  $ret=str_replace("\"","&quot;",$str);
  if($both)
    $ret=str_replace("'","\'",$ret);
  return $ret;
}

function js_escape_quotes($str,$both)
{
  $ret=str_replace("\"","\\\"",$str);
  if($both)
    $ret=str_replace("'","\'",$ret);
  return $ret;
}

function CallAPI($url, $data = false)
{
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_POST, 1);

    if ($data)
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

    /*
    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");
    */
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    
    $result = curl_exec($curl);
    $status = curl_getinfo($curl,  CURLINFO_HTTP_CODE);
    
    curl_close($curl);
    
    $ret = array($result,$status);
    
    return $ret;
}

function getData($url, $data = false)
{
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_HTTPGET, 1);

    $data_str = "?";
    $first = true;
    
    if ($data)
    {
      foreach($data as $key => $value)
      {
        if(!$first)
        { 
          $data_str .= "&";
        }
        else
        {
          $first = false;
        }
        
        $data_str .= $key.'='.urlencode($value);  
      }
    }
    /*
    // Optional Authentication:
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "username:password");
    */
    curl_setopt($curl, CURLOPT_URL, $url.$data_str);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
    
    $result = curl_exec($curl);
    $status = curl_getinfo($curl,  CURLINFO_HTTP_CODE);
    
    curl_close($curl);
    
    $ret = array($result,$status);
    
    return $ret;
}

$odd = true;
        
function newItem($name,$id)
{
  $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
  switch ($lang){
    case "fr":
      $html = array("taux","Créditi:","Taux","Résultats","TAUX");
    break;

    case "it":
      $html = array("vota","Crediti:","Vota","Risultati","VOTA");
    break;

    case "ar":
      $html = array("معدل","االنقاط:","امعدل","النتائج","امعدل");
    break;

    case "zh":
      $html = array("率","积分：","率","结果","率");
    break;

    case "ko":
      $html = array("율","크레딧 :","율","결과","율");
    break;

    default:
      $html = array("voting","Credits:","Voting","Results","VOTE");
    break;
  }

  GLOBAL $odd;
  $new_item = '<div class="list_item ';
  if($odd)
  { 
    $new_item .= 'list_light';
  }
  else
  {
    $new_item .= 'list_dark';
  }
  $odd = !$odd;
  
  $new_item .= '"> <div class="item_wrapper">';
  
  $new_item .= '<div class="blue_button vote_button" rel="'.$id.'" name="'.$name.'">   
                  <span class="left_end"></span><span class="button_text">'.$html[4].'</span><span class="right_end"></span>    
                </div>';
  /*
  $name_arr = explode(" ",$name);
  $first_name = array_shift($name_arr);
  $rest_name = implode(" ",$name_arr);
  */
  $new_item .= '<img src="https://graph.facebook.com/'.$id.'/picture" alt="'.$name.'"> <div class="name">'.$name.'</div>';             
  $new_item .= '</div> <div class="list_line"> </div> </div>';
  
  return $new_item;  
}

function newItemButton($name,$id)
{
  GLOBAL $odd;
  $new_item = '<div class="list_item ';
  if($odd)
  { 
    $new_item .= 'list_light';
  }
  else
  {
    $new_item .= 'list_dark';
  }
  $odd = !$odd;
  
  $new_item .= '"> <div class="item_wrapper">';
  
  $new_item .= '<button class="blue_button" value="'.$id.'&'.$name.'" name="whovote">   
                  VOTE    
                </button>';
  /*
  $name_arr = explode(" ",$name);
  $first_name = array_shift($name_arr);
  $rest_name = implode(" ",$name_arr);
  */
  $new_item .= '<img src="https://graph.facebook.com/'.$id.'/picture" alt="'.$name.'"> <div class="name">'.$name.'</div>';             
  $new_item .= '</div> <div class="list_line"> </div> </div>';
  
  return $new_item;  
}

function newEmptyItem()
{
  GLOBAL $odd;
  $new_item = '<div class="list_item ';
  if($odd)
  { 
    $new_item .= 'list_light';
  }
  else
  {
    $new_item .= 'list_dark';
  }
  $odd = !$odd;
  
  $new_item .= '"> <div class="item_wrapper">';
  
  $new_item .= '<img src="imgs/empty.png" alt="empty"> <span class="name"><br></span>';             
  $new_item .= '</div> <div class="list_line"> </div> </div>';
  
  return $new_item;  
}


// Enforce https on production
if (substr(AppInfo::getUrl(), 0, 8) != 'https://' && $_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
  header('Location: https://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  exit();
}

require_once('sdk/src/facebook.php');

$facebook = new Facebook(array(
  'appId'  => AppInfo::appID(),
  'secret' => AppInfo::appSecret(),
  'sharedSession' => true,
  'trustForwarded' => true,
));

//var_dump($_SESSION['logged_user']);

$access_token = "";
$user_id = $facebook->getUser();
//var_dump($user_id);
if ($user_id) {
  try {
    // Fetch the viewer's basic information
    $basic = $facebook->api('/me');
    $access_token = $facebook->getAccessToken();
  } catch (FacebookApiException $e) {
    // If the call fails we check if we still have a user. The user will be
    // cleared if the error is because of an invalid accesstoken
    if (!$facebook->getUser()) {
      header('Location: '. AppInfo::getUrl($_SERVER['REQUEST_URI']));
      exit();
    }
  }
}
else
{
  $basic = array();
  //$_SESSION['logged_user'] = "";
}

if(!isset($_SESSION['regdata']) || $_SESSION['regdata'] === array())
{
  $_SESSION['regdata'] = $basic;
  $_SESSION['regdataOK'] = false;
}

if(!isset($_SESSION['logged_user']))
{
  $_SESSION['logged_user'] = "";
}

//var_dump($_SESSION['logged_user']);

$arr = explode("/",$_SERVER['REQUEST_URI']);
$pagename = array_pop($arr);
$addr = implode("/",$arr);


if(!($pagename == "login.php" || $pagename == "mainlist.php" || $pagename == "registration.php" || $pagename == "register.php" || $pagename == "terms.php" || $pagename == "senddata.php" || $pagename == "callback.php") && $_SESSION['logged_user'] == "")
{
  header('Location: '. AppInfo::getUrl($addr.'/login.php'));
}

// Fetch the basic info of the app that they are using
$app_info = $facebook->api('/'. AppInfo::appID());

$app_name = idx($app_info, 'name', '');

if(!isset($_SESSION['test']))
{
  $_SESSION['test'] = "0123456789";
}
else
{
  $_SESSION['test'] = substr($_SESSION['test'],0,-1);
}

//var_dump($_SESSION['test']);

?>
