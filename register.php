<?php
  require_once('phpheader.php'); 

  if(he($_POST['first_name']) !== "")
  {
    $_SESSION['regdata']['first_name'] = he($_POST['first_name']);
    $ok['first_name'] = true;
  }
  else
  {
    $ok['first_name'] = false;
  }
  
  if(he($_POST['last_name']) !== "")
  {
    $_SESSION['regdata']['last_name'] = he($_POST['last_name']);
    $ok['last_name'] = true;
  }
  else
  {
    $ok['last_name'] = false;
  }
  
  if(he($_POST['country']) !== "")
  {
    $_SESSION['regdata']['country'] = he($_POST['country']);
    $ok['country'] = true;
  }
  else
  {
    $ok['country'] = false;
  }
  
  if(ctype_digit(he($_POST['day'])) && strlen(he($_POST['day'])) <= 2)
  {
    $birthday['day'] = he($_POST['day']);
    if(strlen($birthday['day']) === 1)
    {
      $birthday['day'] = "0" . $birthday['day'];  
    }
    $ok['day'] = true;
  }
  else
  {
    $ok['day'] = false;
  }

  if(ctype_digit(he($_POST['month'])) && strlen(he($_POST['month'])) <= 2)
  {
    $birthday['month'] = he($_POST['month']);
    if(strlen($birthday['month']) === 1)
    {
      $birthday['month'] = "0" . $birthday['month'];  
    }
    $ok['month'] = true;
  }
  else
  {
    $ok['month'] = false;
  }
  
  if(ctype_digit(he($_POST['year'])) && strlen(he($_POST['year'])) === 4)
  {
    $birthday['year'] = he($_POST['year']);
    $ok['year'] = true;
  }
  else
  {
    $ok['year'] = false;
  }
  
  $ok['birthday'] = $ok['day'] && $ok['month'] && $ok['year'];
  
  if($ok['birthday'])
  {
    $_SESSION['regdata']['birthday'] = $birthday['month'] . "/" . $birthday['day'] . "/" . $birthday['year'];  
    
    $_SESSION['regdata']['birthdayJS'] = $birthday['day'] . "/" . $birthday['month'] . "/" . $birthday['year'];
  }
  
  if(isset($_POST['gender']))
  {
    if(he($_POST['gender']) === "male" || he($_POST['gender']) === "female")
    {
      $_SESSION['regdata']['gender'] = he($_POST['gender']);
      $ok['gender'] = true;
    }
    else
    {
      $ok['gender'] = false;
    }
  }
  else
  {
    $ok['sex'] = false;
  }
  
  if(filter_var(he($_POST['email']),FILTER_VALIDATE_EMAIL) === false)
  {
    $ok['email'] = false;
  }
  else
  {
    $_SESSION['regdata']['email'] = he($_POST['email']);
    $ok['email'] = true;
  }
  
  if($ok['email'] && he($_POST['email']) == he($_POST['confemail']))
  {
    $_SESSION['regdata']['confemail'] = he($_POST['confemail']);
    $ok['confemail'] = true;
  }
  else
  {
    $ok['confemail'] = false;
  }
  
  if(he($_POST['password']) !== "")
  {
    $_SESSION['regdata']['password'] = he($_POST['password']);
    $ok['password'] = true;
  }
  else
  {
    $ok['password'] = false;
  }
  
  if($ok['password'] && he($_POST['password']) == he($_POST['confpassword']))
  {
    $_SESSION['regdata']['confpassword'] = he($_POST['confpassword']);
    $ok['confpassword'] = true;
  }
  else
  {
    $ok['confpassword'] = false;
  }
  
  $_SESSION['regdataOK'] = $ok['first_name'] && $ok['last_name'] && $ok['country'] && $ok['birthday'] && $ok['gender'] && $ok['email'] && $ok['confemail'] && $ok['password'] && $ok['confpassword'];
  $_SESSION['ok'] = $ok;
  
?>

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title>
      registration
    </title>
    <link rel="stylesheet" type="text/css" href="regstyle.css">
    <script src="jquery-1.9.1.min.js" type="text/javascript">
    </script>
  </head>
  <body>
    <?php require_once('fbheader.php'); ?>
    <script type="text/javascript">
      <?php if($_SESSION['regdataOK']) { ?>
        $(document).ready(function ()
          {
            window.location.replace('terms.php');
          });
      <?php } else { ?>
        $(document).ready(function ()
          {
            window.location.assign('registration.php');
          });  
      <?php } ?>
    </script>
  </body>
</html>